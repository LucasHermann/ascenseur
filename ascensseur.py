from dataclasses import dataclass

@dataclass
class Porte:
    pass

@dataclass
class Bouton:
    appuye: bool = False

@dataclass
class BoutonEtage(Bouton):
    numero_etage: int = 0

@dataclass
class Etage:
    bouton: Bouton

@dataclass
class Ascenseur:
    boutons: list[Etage]

    def aller_a(self):
        pass

    def recuperer_etage(self):
        pass

@dataclass
class Batiment:
    etages: list[Etage]
    ascenseur: Ascenseur


def main():
    batiment = Batiment()

import unittest

class TestAscenseur(unittest.TestCase):

    def setUp(self):
        self.nombre_etage = 2
        self.etages = [Etage(nb) for nb in range(self.nombre_etage)]
        self.ascenseur = Ascenseur(
            boutons=self.etages
        )
        self.batiment = Batiment(
            etages=self.etages,
            ascenseur=self.ascenseur
        )

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

if __name__ == '__main__':
    unittest.main()
